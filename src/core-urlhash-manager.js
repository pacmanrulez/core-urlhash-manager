/*
 *     Date: 2023
 *  Package: core-urlhash-manager
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-urlhash-manager
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

module.exports = (function () {

    const PACKAGE = 'HashManager';

    if(!document) {
        throw new Error('Should run a in an environment where DOMDocument is available.');
    }

    const
        CHANGE_EVENT = 'hashchange',
        WINDOW = window,
        DOCUMENT = document,
        GLOBAL = 'core', // global object name,
        DOM = require('core-utilities/utility/dom'),
        ElementScroller = require('core-element-scroller')
    ;

    let HashManager = function() {};

    HashManager.track = function(options) {
        options = {
            ...{
                query: '[data-hash-tracker="?"]',
                clearHash: false,
                Scroller: {},
            },
            ...options
        }

        WINDOW.addEventListener(CHANGE_EVENT, function() {
            let hash = HashManager.getHash();
            if(hash === false) {
                HashManager.clearHash();
                return;
            }
            let query = options.query.replace('?', hash);
            ElementScroller.scrollTo(query, options.Scroller);
            // before we remove the hash, we wait till so the scroller can take over.
            setTimeout(function() {
                if(options.clearHash) {
                    HashManager.clearHash();
                }
            }, 200)
        });

        let triggers = DOM.get(['[data-hash-href]']);
        if(triggers) {
            triggers.forEach((trigger) => {
                trigger.addEventListener('click', () => {
                    HashManager.setHash(trigger.dataset.hashHref);
                })
            });
        }

        if(HashManager.getHash() !== false) {
            WINDOW.dispatchEvent(new Event(CHANGE_EVENT));
        }
    };
    
    /**
     *
     * @param {String} hash
     * @returns {boolean}
     */
    HashManager.isHash = function(hash) {
        // only accept strings
        if(typeof hash !== 'string') {
            return false;
        }
        return HashManager.getHash() === hash.replace('#', '');
    };

    HashManager.getHash = function() {
        let current = WINDOW.location.hash.replace('#', '');
        return current !== '' ? decodeURIComponent(current) : false;
    };

    HashManager.setHash = function(hash) {
        // HTML 5 API
        if(typeof history !== 'undefined') {
            // backwards compatibility with older browsers
            history.pushState(hash, DOCUMENT.title, WINDOW.location.pathname
                + WINDOW.location.search);
            // newer browsers require full URL to satisfy securty checks
            history.replaceState(hash, DOCUMENT.title, WINDOW.location.origin
                + WINDOW.location.pathname + WINDOW.location.search);
        }

        if(hash.trim() === '') {
            return;
        }

        // if not supported, default, still includes the hash (#) symbol
        return WINDOW.location.hash = hash;
    }

    HashManager.isBlankHash = function() {
        return WINDOW.location.href.indexOf('#') === (WINDOW.location.href.length -1);
    }

    /**
     * if hashes contains current hash
     * @param {Array} hashes
     * @returns {boolean}
     */
    HashManager.hasHash = function(hashes) {
        // create array if none
        if(!Array.isArray(hashes)) {
            hashes = [hashes];
        }
        // return true if as match was found
        for(let index in hashes) {
            let hash = hashes[index];
            if(HashManager.isHash(hash)) {
                // return
                return true;
            }
        }
        // no match
        return false;
    };

    /**
     * clear current hash from URL
     * @returns {string}
     */
    HashManager.clearHash = function() {
        return HashManager.setHash('');
    };

    /**
     * prevent default behaviour where an empty hash (#) will scroll to top (blank).
     */
    HashManager.preventScrollingToBlank = function() {
        WINDOW.addEventListener(CHANGE_EVENT, () => {
            if(!HashManager.isBlankHash()) {
                return;
            }
            let position = WINDOW.scrollY;
            WINDOW.scroll(0, position);
            HashManager.clearHash();
        });
    }

    // assign to global object core
    if(WINDOW) {
        if(!WINDOW[GLOBAL]) { WINDOW[GLOBAL] = { }; }
        if(!WINDOW[GLOBAL][PACKAGE]) {
            WINDOW[GLOBAL][PACKAGE] = HashManager;
        }
    }

    return HashManager;
})();