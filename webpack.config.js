/*
 *     Date: 2023
 *  Package: core-urlhash-manager
 *   Author: dbf, pacmanrulez, wize-wiz, wizdom
 *   Source: https://gitlab.com/pacmanrulez/core-urlhash-manager
 *  License: WTFPL v2 (http://www.wtfpl.net/txt/copying/)
 *
 *
 */

const path = require('path');

module.exports = {
  mode: 'production',
  entry: './src/core-urlhash-manager.js',
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'core-urlhash-manager.js',
    library: "coreUrlHashManager",
    libraryTarget: 'umd',
    globalObject: 'this'
  }
};
